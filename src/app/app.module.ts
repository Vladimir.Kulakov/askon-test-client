import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProfileViewComponent } from './components/profile-view/profile-view.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { SmallCardLayoutComponent } from './layouts/small-card-layout/small-card-layout.component';
import { LargeCardLayoutComponent } from './layouts/large-card-layout/large-card-layout.component';
import { MaterialModule } from './modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { GridElementSelectorComponent } from './components/grid-element-selector/grid-element-selector.component';
import { isEditingPipe } from './components/profile-view/pipes/is-editing.pipe';
import { GridElementFieldComponent } from './components/grid-element-field/grid-element-field.component';
import { GridElementComponent } from './components/grid-element/grid-element.component';
import { FormsModule } from '@angular/forms';
import { GridElementImageComponent } from './components/grid-element-image/grid-element-image.component';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ProfileViewComponent,
    NotFoundComponent,
    SmallCardLayoutComponent,
    LargeCardLayoutComponent,
    GridElementSelectorComponent,
    isEditingPipe,
    GridElementFieldComponent,
    GridElementComponent,
    GridElementImageComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { subscriptSizing: 'dynamic' } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
