import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridElementFieldComponent } from './grid-element-field.component';

describe('GridElementFieldComponent', () => {
  let component: GridElementFieldComponent;
  let fixture: ComponentFixture<GridElementFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridElementFieldComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GridElementFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
