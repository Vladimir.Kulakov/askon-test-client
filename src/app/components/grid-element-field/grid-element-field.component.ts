import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserInfo } from 'src/app/dto/user-response.dto';
import { UserInfoService } from 'src/app/services/user-info.service';
import { FieldValueType } from '../profile-view/type/selector-value.type';

@Component({
  selector: 'app-grid-element-field',
  templateUrl: './grid-element-field.component.html',
  styleUrls: ['./grid-element-field.component.scss'],
})
export class GridElementFieldComponent implements OnChanges, OnDestroy {
  @Input() disabled!: boolean;
  @Input() title?: string;
  @Input() field!: FieldValueType;
  
  private readonly subs: Subscription[] = [];

  public value: any = '';
  constructor(private readonly userInfoService: UserInfoService) {
    this.subs.push(this.userInfoService.obs.subscribe((value)=> {
      this.getValue();
    }));
  }

  private getValue() {
    this.value = this.userInfoService.getValue(this.field.title as keyof UserInfo) || '';
  }

  public onValueChange(value: string) {
    this.userInfoService.setValue(this.field.title as keyof UserInfo, value)
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['field']?.currentValue) {
      this.getValue();
    }
  }

  ngOnDestroy(): void {
    this.subs.forEach((sub)=> {
      sub.unsubscribe();
    })
  }

}
