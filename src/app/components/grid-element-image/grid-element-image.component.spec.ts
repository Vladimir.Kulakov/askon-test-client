import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridElementImageComponent } from './grid-element-image.component';

describe('GridElementImageComponent', () => {
  let component: GridElementImageComponent;
  let fixture: ComponentFixture<GridElementImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridElementImageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GridElementImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
