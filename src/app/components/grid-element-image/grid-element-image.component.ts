import { Component, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserInfoService } from 'src/app/services/user-info.service';

@Component({
  selector: 'app-grid-element-image',
  templateUrl: './grid-element-image.component.html',
  styleUrls: ['./grid-element-image.component.scss']
})
export class GridElementImageComponent {
  @Input() disabled!: boolean;
  
  private readonly subs: Subscription[] = [];

  public value: any = '';
  constructor(private readonly userInfoService: UserInfoService) {
    this.getValue();
    this.subs.push(this.userInfoService.obs.subscribe((value)=> {
      this.getValue();
    }));
  }

  private getValue() {
    this.value = `${(this.userInfoService.getValue('avatar') || '')}?${Date.now()}`;
  }

  public onValueChange(event: Event) {
    const target = event.target as HTMLInputElement;
    const files = target.files;
    this.userInfoService.setAvatar(files![0])
  }

  ngOnDestroy(): void {
    this.subs.forEach((sub)=> {
      sub.unsubscribe();
    })
  }
}
