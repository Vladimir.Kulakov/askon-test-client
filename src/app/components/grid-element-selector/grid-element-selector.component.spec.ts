import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridElementSelectorComponent } from './grid-element-selector.component';

describe('GridElementSelectorComponent', () => {
  let component: GridElementSelectorComponent;
  let fixture: ComponentFixture<GridElementSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridElementSelectorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GridElementSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
