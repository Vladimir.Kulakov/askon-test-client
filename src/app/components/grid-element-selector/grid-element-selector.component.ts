import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { UserInfo } from 'src/app/dto/user-response.dto';
import { GridElement } from '../profile-view/class/element.class';

@Component({
  selector: 'app-grid-element-selector',
  templateUrl: './grid-element-selector.component.html',
  styleUrls: ['./grid-element-selector.component.scss']
})
export class GridElementSelectorComponent {
  public types = GridElement.getAllowedTypes();
  public fields = UserInfo.getFields();

  public readonly typeControl = new FormControl();
  public readonly fieldControl = new FormControl(this.fields[0]);

  constructor(private readonly dialogRef: MatDialogRef<string>) {}

  public onClick() {
    this.dialogRef.close({
      type: this.typeControl.value,
      field: this.fieldControl.value
    })
  }
}
