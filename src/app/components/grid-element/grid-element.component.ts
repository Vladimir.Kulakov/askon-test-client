import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { ElementType } from '../profile-view/class/element.class';
import { FieldValueType } from '../profile-view/type/selector-value.type';

@Component({
  selector: 'app-grid-element',
  templateUrl: './grid-element.component.html',
  styleUrls: ['./grid-element.component.scss']
})
export class GridElementComponent {
  @ViewChild('contentArea') contentArea!: ElementRef;
  @Input() disabled: boolean = true;
  @Input() field?: FieldValueType;
  @Input() type!: ElementType;
  @Output() onDelete = new EventEmitter();
}
