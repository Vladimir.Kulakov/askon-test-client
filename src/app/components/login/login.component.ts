import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Routes } from 'src/app/services/routes';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public isPasswordHidden = true;
  public isLoading = false;
  public loginForm = new FormGroup({
    login: new FormControl('', [Validators.required, Validators.maxLength(10)]),
    password: new FormControl('', [Validators.required, Validators.maxLength(10)]),
  })

  constructor(private readonly authService: AuthService, private readonly router: Router) {}

  public login() {
    if (!this.loginForm.valid) {
      return;
    }
    const {login, password} = this.loginForm.value;
    this.isLoading = true;
    this.authService.login(login!, password!)
    .catch((error)=> {
      console.error(error);
      this.setIncorrect();
    })
    .finally(()=> {
      this.isLoading = false;
    })
  }

  private setIncorrect(value = true) {
    this.loginForm.controls.login.setErrors({incorrect: value});
    this.loginForm.controls.password.setErrors({incorrect: value});
  }

  public onInputClick() {
    if (!this.loginForm.value.login || !this.loginForm.value.password) {
      return;
    }
    this.loginForm.controls.login.updateValueAndValidity();
    this.loginForm.controls.password.updateValueAndValidity();
  }
}
