import { Point } from "@angular/cdk/drag-drop";
import { ComponentRef } from "@angular/core";
import { v4 as uuidv4 } from 'uuid';

export class GridElement {

    private readonly _id: string;
    public get id() {
        return this._id;
    }

    private readonly _dragable: any;
    public get dragable() {
        return this._dragable;
    }

    private readonly _component: ComponentRef<any>;
    public get component() {
        return this._component;
    }

    private _index!: Point[] | null;
    public get index() {
        return this._index;
    }

    private readonly _type: GridElementType;
    public get type() {
        return this._type;
    }

    private readonly _field?: string;
    public get field() {
        return this._field;
    }

    private readonly _viewField?: string;
    public get viewField() {
        return this._viewField;
    }



    constructor(
        dragable: any, 
        type: GridElementType, 
        component: ComponentRef<any>, 
        field?: string, 
        viewField?: string
    ) {
        this._id = uuidv4();
        this._dragable = dragable;
        this._type = type;
        this._component = component;
        this._field = field;
        this._viewField = viewField;
    }

    public setIndex(index: Point[] | null) {
        this._index = index;
    }
    public hasIndex(index: Point) {
        return this._index?.find(i=> {
            return i.x == index.x && i.y == index.y;
        })
    }

    static getAllowedTypes() {
        return [
            new GridElementType(ElementType.FIELD, {x: 1, y: 1}, false, 'Поле'),
            new GridElementType(ElementType.TABLE, {x: 3, y: 3}, true, 'Таблица'),
            new GridElementType(ElementType.IMAGE, {x: 2, y: 2}, true, 'Изображение'),
            new GridElementType(ElementType.CONTAINER, {x: 3, y: 3}, true, 'Контейнер'),
        ]
    }

    public getTemplate(): GridElementTemplate {
        return {
            index: this.index!,
            type: this.type.title,
            size: this.type.size,        
            field: this.field,
        };
    }
}

export type GridElementTemplate = {
    index: Point[],
    type: string,
    size: Point,  
    field?: string,
    viewField?: string,
}

export enum ElementType {
    FIELD = 'field',
    TABLE = 'table',
    IMAGE = 'image',
    CONTAINER = 'container',
}

export class GridElementType {
    constructor(
        public readonly title: ElementType,
        public readonly size: Point,
        public readonly container?: boolean,
        public readonly viewTitle?: string,
    ) {}
}