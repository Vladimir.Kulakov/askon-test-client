import { Point } from "@angular/cdk/drag-drop";
import { GridElement } from "./element.class";

export class Grid {
    private readonly space: GridElement[] = [];
    private readonly sizeH = 6;
    private readonly sizeW = 4;
    private readonly gap = 10;
    private readonly _cellSize = {
      h: 0, w:0, vH: 0, vW:0, 
    }
    public get cellSize() {
        return this._cellSize;
    }

    constructor(nativeElement: any) {
        this.initGridParts(nativeElement);
    }
    
    private initGridParts(nativeElement: any) {
        const cH = nativeElement.clientHeight;
        const cW = nativeElement.clientWidth; 
        this._cellSize.h = (cH / this.sizeH >> 0);
        this._cellSize.w = (cW / this.sizeW >> 0);
        this._cellSize.vH = this._cellSize.h - this.gap;
        this._cellSize.vW = this._cellSize.w - this.gap;
    }

    private _lock(flag: boolean) {
        this.space.forEach((elem)=> {
            elem.component.setInput('disabled', flag);
            elem.dragable.disabled = flag;
        })
    }

    public lock() {
        this._lock(true);
    }
    
    public unlock() {
        this._lock(false);
    }

    public setElement(element: GridElement) {
        this.space.push(element);
    }

    public deleteElement(id: string) {
        const elementIndex = this.space.findIndex(element => element.id === id) 
        if (elementIndex === -1) {
            console.error('element by id not found');
            return;
        }
        this.space.splice(elementIndex, 1);
    }

    public findElementOnIndex(index:Point) {
        return this.space.find((elem)=> {
           return elem.hasIndex(index);
        })
    } 
    
    public getNearestFreeGridIndex(target:Point, forSize: Point) {
        const targetIndex = this.getGridCellIndex(target);
        let result = this.checkIndexOnFit(targetIndex, forSize);
        if (result) {
            return result;
        }
        const fitable = [];
        for (let i = 0; i < this.sizeW; i++) {
            for (let j = 0; j < this.sizeH; j++) {
                const fit = this.checkIndexOnFit({x:i, y:j}, forSize);
                if (fit) {
                    fitable.push(fit);
                }
            }
        }
        if (!fitable.length) {
            return false;
        }
        let min = getDistance(targetIndex, fitable[0][0]);
        result = fitable[0];
        for (let i = 1; i< fitable.length; i++) {
            const distance = getDistance(targetIndex, fitable[i][0]);
            if (distance < min) {
                min = distance;
                result = fitable[i];
            }
        }

        return result;

    }

    private checkIndexOnFit(index: Point, forSize: Point) {
        const borderX = index.x + forSize.x;
        const borderY = index.y + forSize.y;
        if ( borderX > this.sizeW || borderY > this.sizeH) {
            return false;
        }
        const result = [];
        for (let i = index.x; i < borderX; i++) {
            for (let j = index.y; j < borderY; j++) {
                const checking = {x: i, y: j};
                if (!this.findElementOnIndex(checking)) {
                    result.push(checking);
                } else {
                    return false;
                }
            }
        }
        return result;
    }

    public fromIndexToPoint(index: Point) {
        return {
            x: index.x * this._cellSize.w,
            y: index.y * this._cellSize.h,
        };
    }

    public getNearestGridPoint({x,y}:Point) {
    return {x: x - (x % this._cellSize.w), y: (y - y % this._cellSize.h)};
    }

    public getGridCellIndex({x,y}:Point) {
        const point = this.getNearestGridPoint({x,y});
        return {
            x: point.x / this._cellSize.w,
            y: point.y / this._cellSize.h,
        }
    }

    public calcSizeElem(size: Point) {
        return {
            x: size.x * this._cellSize.vW + ((size.x - 1) * this.gap),
            y: size.y * this._cellSize.vH + ((size.y - 1) * this.gap),
        }
    }

    public getTemplate() {
        return (
            this.space.map(element=> element.getTemplate())
        );
    }
}
 
function fromPoint(p: Point) {
    return {
        x: p.x,
        y: p.y,
    }
}

function getDistance(p: Point, p1: Point) {
    return Math.sqrt(Math.pow(p.x - p1.x, 2) + Math.pow(p.y - p1.y, 2));
}