import { Pipe, PipeTransform } from "@angular/core";
import { FormStatus } from "../profile-view.component";

@Pipe({name: 'isEditing'})
export class isEditingPipe implements PipeTransform {
    transform(currentStatus: FormStatus) {
        return currentStatus === FormStatus.EDIT_FORM;
    }
}