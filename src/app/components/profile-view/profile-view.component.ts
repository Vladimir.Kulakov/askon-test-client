import { DragDrop, Point } from '@angular/cdk/drag-drop';
import { Component, ElementRef, OnInit, ViewChild, ViewContainerRef, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { User, UserInfo } from 'src/app/dto/user-response.dto';
import { LoginService } from 'src/app/services/login.service';
import { UserInfoService } from 'src/app/services/user-info.service';
import { GridElementSelectorComponent } from '../grid-element-selector/grid-element-selector.component';
import { GridElementComponent } from '../grid-element/grid-element.component';
import { ElementType, GridElement, GridElementTemplate, GridElementType } from './class/element.class';
import { Grid } from './class/grid.class';
import { SelectorValueType } from './type/selector-value.type';

export enum FormStatus {
  READONLY, EDIT_INFO, EDIT_FORM,
}

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
export class ProfileViewComponent implements OnInit, AfterViewInit {
  public user: User | null = null;
  private jAreaElem!: JQuery<any>;
  private grid!: Grid;
  public currentStatus:FormStatus = FormStatus.READONLY;
  private isDragging:boolean = false;
  private readonly isMe = this.router.snapshot.url.find(url=> url.path === 'me');
  public readonly nick = this.router.snapshot.params['nick'];
  public readonly canEditForm = this.isMe;
  public readonly canEditInfo = this.isMe;
  public isLoading = true;
  private get isEditForm() {
    return this.currentStatus === FormStatus.EDIT_FORM;
  }

  @ViewChild('gridArea', {read: ViewContainerRef}) gridArea!: ViewContainerRef;
  @ViewChild('contentArea') contentArea!: ElementRef;

  constructor(
    private readonly userService: UserInfoService,
    private readonly loginService: LoginService,
    private readonly dragService: DragDrop,
    private readonly dialog: MatDialog,
    private readonly router: ActivatedRoute,
  ) {}

  ngAfterViewInit(): void {
    this.grid = new Grid(this.contentArea.nativeElement);
    this.jAreaElem = $(this.contentArea.nativeElement);
  }

  ngOnInit(): void {
    this.loadData();
  }

  private loadData() {
    this.isLoading = true;
    this.userService.load(this.nick).finally(()=> {
      const template = this.userService.template;
      template?.forEach((element: GridElementTemplate) => {
        this.restoreFromTemplate(element);
      });
      this.isLoading = false;
    })
  }

  private setStatus(status: FormStatus) {
    this.currentStatus = status;
  }

  public onEditClick() {
    this.setStatus(FormStatus.EDIT_FORM);
    this.grid.unlock();
  }

  public onSaveClick() {
    this.setStatus(FormStatus.READONLY);
    this.grid.lock();
    this.userService.updateInfo()?.then(()=>{
      this.loadData();
    });
    this.userService.updateTemplate(this.grid.getTemplate());
  }

  public onLogoutClick() {
    this.loginService.logOut();
  }

  public onContentFieldClick(event: MouseEvent) {
    if (!this.canEditForm) {
      return;
    }
    if (!this.isEditForm) {
      return;
    }
    if (this.isDragging) {
      return;
    }

    const dialogRef = this.dialog.open(GridElementSelectorComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.createNewDrag(event, result);
      }
    });
  }
  
  private createNewDrag(event: MouseEvent, data: SelectorValueType) {
    event.preventDefault();
    const clickPoint = this.fromGlobalToLocal({
      x: event.pageX,
      y: event.pageY,
    })
    const freeIndex = this.getFreeIndex(clickPoint, data.type.size);
    if (!freeIndex) {
      return;
    }

    const dragElem = this.initDragElement(data);
    this.placeDrag(dragElem, freeIndex);
  }

  private restoreFromTemplate(template: GridElementTemplate) {
    const fields = UserInfo.getFields();
    const dragElem = this.initDragElement({
      type: new GridElementType(template.type as ElementType, template.size),
      field: fields.find(field=> (field.title === template.field)),
    });
    this.placeDrag(dragElem, template.index);
  }


  private initDragElement(data: SelectorValueType) {
    const componentRef = this.gridArea.createComponent<GridElementComponent>(GridElementComponent);
    componentRef.setInput('disabled', !this.isEditForm);
    componentRef.setInput('type', data.type.title);
    componentRef.setInput('field', data.field);
    firstValueFrom(componentRef.instance.onDelete).then(()=> {
      componentRef.destroy();
      this.grid.deleteElement(dragElem.id);
    })
    const size = this.grid.calcSizeElem(data.type.size);
    $(componentRef.location.nativeElement)
    .css('width', size.x)
    .css('height', size.y)
    .on('click', (event)=> {
      event.stopPropagation();
    });
    const dragable = this.dragService.createDrag(componentRef.location.nativeElement, {
      dragStartThreshold: 10,
      pointerDirectionChangeThreshold: 10,
    });
    dragable.withBoundaryElement(this.contentArea.nativeElement);
    dragable.disabled = !this.isEditForm;

    const dragElem = new GridElement(
      dragable, 
      data.type, 
      componentRef, 
      data.field?.title, 
      data.field?.viewTitle
    );
    dragable.ended.subscribe((event)=> {
      const clickPoint = this.fromGlobalToLocal({
        x: event.dropPoint.x,
        y: event.dropPoint.y,
      })
      const freeIndex = this.getFreeIndex(clickPoint, data.type.size)
      if (freeIndex) { 
        this.placeDrag(dragElem, freeIndex);
      }
      this.isDragging = false;
    })
    dragable.started.subscribe(()=> {
      dragElem.setIndex(null);
      this.isDragging = true;
    })

    this.grid.setElement(dragElem);
    return dragElem;
  }

  private fromGlobalToLocal(clickPoint: Point) {
    return {
      x : clickPoint.x - (this.jAreaElem.offset()?.left || 0),
      y : clickPoint.y - (this.jAreaElem.offset()?.top || 0),
    };
  }

  private getFreeIndex(clickPoint: Point, size: Point) {
    const nearestGridPoint = this.grid.getNearestGridPoint(clickPoint);
    return this.grid.getNearestFreeGridIndex(nearestGridPoint, size);
  }

  private placeDrag(dragElem: GridElement, index: Point[]) {
    dragElem.setIndex(index);
    dragElem.dragable.setFreeDragPosition(this.grid.fromIndexToPoint(index[0]));
  }
  
}
