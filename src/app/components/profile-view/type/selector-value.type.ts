import { UserInfo } from "src/app/dto/user-response.dto"
import { GridElementType } from "../class/element.class"

export type SelectorValueType = {
    type: GridElementType, 
    field?: FieldValueType
}

export type FieldValueType = {
    title?: string, 
    viewTitle?: string, 
    type?: string
}