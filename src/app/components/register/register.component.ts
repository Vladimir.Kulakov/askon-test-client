import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Routes } from 'src/app/services/routes';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  private readonly regExPattern = /^\D[a-zA-Z0-9_]+$/;
  public isPasswordHidden = true;
  public isLoading = false;
  public registerForm = new FormGroup({
    login: new FormControl('', 
      [
        Validators.required, 
        Validators.maxLength(10),
        Validators.pattern(this.regExPattern),
      ]),
    password: new FormControl('', [Validators.required, Validators.maxLength(10)]),
    nickname: new FormControl('', 
      [
        Validators.required, 
        Validators.maxLength(10), 
        Validators.pattern(this.regExPattern),
      ]),
  })

  constructor(private readonly authService: AuthService, private readonly router: Router) {}

  private setIncorrect(value = true) {
    this.registerForm.controls.login.setErrors({incorrect: value});
    this.registerForm.controls.password.setErrors({incorrect: value});
    this.registerForm.controls.nickname.setErrors({incorrect: value});
  }

  public register() {
    if (!this.registerForm.valid) {
      return;
    }
    const {login, password, nickname} = this.registerForm.value;
    this.isLoading = true;
    this.authService.register(login!, password!, nickname!)
    .then(()=> {
      this.router.navigate([Routes.LOGIN]);
    })
    .catch((error)=> {
      console.error(error);
      this.setIncorrect();
    })
    .finally(()=> {
      this.isLoading = false;
    })
  }

  public onInputClick() {
    const {login, password, nickname} = this.registerForm.value;
    if (!login || !password || !nickname) {
      return;
    }
    this.registerForm.controls.login.updateValueAndValidity();
    this.registerForm.controls.password.updateValueAndValidity();
    this.registerForm.controls.nickname.updateValueAndValidity();
  }

}
