import { HttpStatusCode } from "@angular/common/http"

export type BaseResponseDto = {
    statusCode: HttpStatusCode;
    value: any;
}