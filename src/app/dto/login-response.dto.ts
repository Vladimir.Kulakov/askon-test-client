import { BaseResponseDto } from "./base-response.dto";

export interface LoginResponseDto extends BaseResponseDto {
    value: {
        token: string;
        expired: string;
        nickName: string;
    };
}