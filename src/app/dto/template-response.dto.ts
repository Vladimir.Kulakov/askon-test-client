import { GridElementTemplate } from "../components/profile-view/class/element.class";
import { BaseResponseDto } from "./base-response.dto";

export interface TemplateResponseDto extends BaseResponseDto {
    value: GridElementTemplate[];
}
