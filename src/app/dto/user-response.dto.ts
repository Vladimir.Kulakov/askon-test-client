import { BaseResponseDto } from "./base-response.dto";

export interface UserResponseDto extends BaseResponseDto {
    value: User;
}

export type User = {
    loginName: string;
    nickName: string;
    userInfo: UserInfo | null;
}

export class UserInfo {
    avatar!: string;
    dateBirth!: string;
    description!: string;
    email!: string;
    firstName!: string;
    lastName!: string;
    phone!: string;

    
    static getFields() {
        return [
            {
                title: 'firstName', viewTitle: 'Имя', type: 'text',
            },
            {
                title: 'lastName', viewTitle: 'Фамилия', type: 'text',
            },
            {
                title: 'dateBirth', viewTitle: 'Дата рождения', type: 'date',
            },
            {
                title: 'phone', viewTitle: 'Номер телефона', type: 'tel',
            },
            {
                title: 'email', viewTitle: 'Электронная почта', type: 'email'
            },
            {
                title: 'description', viewTitle: 'О себе', type: 'text'
            },
        ]
    }
}