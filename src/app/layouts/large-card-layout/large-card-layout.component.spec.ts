import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LargeCardLayoutComponent } from './large-card-layout.component';

describe('LargeCardLayoutComponent', () => {
  let component: LargeCardLayoutComponent;
  let fixture: ComponentFixture<LargeCardLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LargeCardLayoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LargeCardLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
