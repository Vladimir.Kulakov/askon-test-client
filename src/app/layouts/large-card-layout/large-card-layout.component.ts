import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-large-card-layout',
  templateUrl: './large-card-layout.component.html',
  styleUrls: ['./large-card-layout.component.scss']
})
export class LargeCardLayoutComponent {
  @Input() title: string | undefined;
}
