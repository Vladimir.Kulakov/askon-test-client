import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmallCardLayoutComponent } from './small-card-layout.component';

describe('SmallCardLayoutComponent', () => {
  let component: SmallCardLayoutComponent;
  let fixture: ComponentFixture<SmallCardLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmallCardLayoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SmallCardLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
