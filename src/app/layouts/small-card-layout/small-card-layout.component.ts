import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-small-card-layout',
  templateUrl: './small-card-layout.component.html',
  styleUrls: ['./small-card-layout.component.scss']
})
export class SmallCardLayoutComponent {
  @Input() title: string = ''
}
