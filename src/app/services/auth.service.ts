import { Injectable } from '@angular/core';
import { LoginResponseDto } from '../dto/login-response.dto';
import { DataService } from './data.service';
import * as crypto from 'crypto-js';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { Routes } from './routes';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public get isLoggedIn() {
    return this.loginService.isLoggedIn;
  }

  constructor(
    private readonly dataService: DataService,
    private readonly loginService:LoginService,
    private readonly router: Router
  ) {}

  public login(login: string, password: string) {
    const hashedPass = this.getHashPassword(password);
    return this.dataService.login<LoginResponseDto>(login, hashedPass)
    .then((data: LoginResponseDto) => {
      this.loginService.logIn(data.value.token, data.value.nickName);
      this.router.navigate([Routes.ME]);
      return data;
    });
  }

  private getHashPassword(password:string) {
    const hash = crypto.SHA256(password);
    return hash.toString();
  }

  public register(login: string, password: string, nickName: string) {
    const hashedPass = this.getHashPassword(password);
    return this.dataService.register(login, hashedPass, nickName);
  }

}
