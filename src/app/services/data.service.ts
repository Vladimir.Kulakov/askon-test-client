import { HttpClient, HttpStatusCode } from '@angular/common/http';
import { Injectable, isDevMode } from '@angular/core';
import { Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GridElementTemplate } from '../components/profile-view/class/element.class';
import { TemplateResponseDto } from '../dto/template-response.dto';
import { UserResponseDto } from '../dto/user-response.dto';
import { LoginService } from './login.service';
import { Routes } from './routes';
import * as moment from 'moment';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private get token() {
    return this.loginService.token
  }

  constructor(
    private readonly http: HttpClient, 
    private readonly router: Router,
    private readonly loginService: LoginService,
    private readonly snackBar: MatSnackBar,
  ) { }

  private get<T>(url: string, params?: any, token?: string): Promise<T>{
    const headers = token? {
        Authorization: `Bearer ${token}`
    } : undefined;
    return firstValueFrom(this.http.get<T>(url, { params: params, headers }))
    .catch((error: {status: HttpStatusCode})=> {
      throw this.errorHandler(error);
    })
  }

  private post<T>(url: string, data?: any, token?: string): Promise<T>{
    const headers = token? {
        Authorization: `Bearer ${token}`,
    } : undefined
    return firstValueFrom(this.http.post<T>(url, data, {headers}))
    .catch((error: {status: HttpStatusCode})=> {
      throw this.errorHandler(error);
    })
  }

  private put<T>(url: string, data?: any, token?: string): Promise<T>{
    const headers = token? {
        Authorization: `Bearer ${token}`,
    } : undefined
    return firstValueFrom(this.http.put<T>(url, data, {headers}))
    .catch((error: {status: HttpStatusCode})=> {
      throw this.errorHandler(error);
    })
  }

  private getAuthorized<T>(url: string, params?: any) {
    if (!this.token) {
      this.router.navigate([Routes.LOGIN]);
      return;
    }

    return this.get<T>(url, params, this.token);
  }

  private postAuthorized<T>(url: string, data?: any) {
    if (!this.token) {
      this.router.navigate([Routes.LOGIN]);
      return;
    }

    return this.post<T>(url, data, this.token);
  }

  private putAuthorized<T>(url: string, data?: any) {
    if (!this.token) {
      this.router.navigate([Routes.LOGIN]);
      return;
    }

    return this.put<T>(url, data, this.token);
  }

  private errorHandler(error: {status: HttpStatusCode}) {
    switch (error.status) {
      case HttpStatusCode.Unauthorized:
        this.snackBar.open('Unauthorized', '', {
          duration: 1000,
          verticalPosition: 'top',
        });
        this.loginService.logOut();
        break;

        case HttpStatusCode.NotFound:
          this.snackBar.open('Not found', '', {
            duration: 1000,
            verticalPosition: 'top',
          });
          this.router.navigate([Routes.NOT_FOUND]);
        break;
        default:
          break;
    }
    throw error;
  }


  private url(...paths: string[]) {
    const baseUrl = environment.apiUrl;
    return `${baseUrl}${baseUrl.endsWith('/')? '' : '/'}${paths.join('/')}`;
  }

  public login<T>(login: string, password: string) {
    return this.post<T>(this.url(Routes.LOGIN), { login, password });
  }

  public register<T>(login: string, password: string, nickName: string) {
    return this.post<T>(this.url(Routes.REGISTER), { login, password, nickName });
  }

  public getMe() {
    return this.getAuthorized<UserResponseDto>(this.url(Routes.ME))?.then((data)=> {
      return data.value;
    });
  }

  public getUser(nick: string) {
    return this.getAuthorized<UserResponseDto>(this.url(Routes.USER, nick))?.then((data)=> {
      const userInfo = data.value.userInfo;
      if(isDevMode() && userInfo?.avatar) {
        userInfo.avatar = this.url(userInfo.avatar);
      }
      if (userInfo?.dateBirth) {
        userInfo.dateBirth = moment(userInfo.dateBirth).format('yyyy-MM-DD');
      }
      return data.value;
    });
  }

  public getTemplate(nick: string) {
    return this.getAuthorized<TemplateResponseDto>(this.url(Routes.USER, nick, Routes.TEMPLATE))?.then((data)=> {
      return data.value;
    });
  }

  public updateUser(nick: string, data: FormData) {
    return this.putAuthorized(this.url(Routes.USER, nick), data);
  }

  public updateTemplate(nick: string, data: GridElementTemplate[]) {
    return this.putAuthorized(this.url(Routes.USER, nick, Routes.TEMPLATE), data);
  }
}

