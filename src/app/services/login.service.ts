import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Routes } from './routes';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private _isLoggedIn = false;

  private _token: string | null = null;
  private _nickname: string | null = null;

  private readonly tokenKey = 'token';
  private readonly nicknameKey = 'nickname';

  public get isLoggedIn() {
    return this._isLoggedIn;
  }

  public get token() {
    return this._token;
  }

  public get nickname() {
    return this._nickname;
  }

  constructor(private readonly router: Router, private readonly snackBar: MatSnackBar) {
    this.startUpLogin();
  }

  private startUpLogin() {
    const token = this.getToken();
    const nickname = this.getNickname();
    if (!token || !nickname) {
      return this.router.navigate([Routes.LOGIN]);
    }
    return this.logIn(token, nickname);
  }

  public logIn(token: string, nickname: string) {
    this._isLoggedIn = true;
    this.setToken(token);
    this.setNickname(nickname);
    this.snackBar.open('Welcome', '', {
      duration: 1000,
      verticalPosition: 'top',
    });
  }

  public logOut() {
    this._isLoggedIn = false;
    this.clearToken();
    this.router.navigate([Routes.LOGIN]);
  }

  private setToken(token: string) {
    this._token = token;
    return localStorage.setItem(this.tokenKey, token);
  }  
  
  private setNickname(nickName: string) {
    this._nickname = nickName;
    return localStorage.setItem(this.nicknameKey, nickName);
  }

  private clearToken() {
    return localStorage.removeItem(this.tokenKey);
  }

  private getToken() {
    return localStorage.getItem(this.tokenKey);
  }

  private getNickname() {
    return localStorage.getItem(this.nicknameKey);
  }
}
