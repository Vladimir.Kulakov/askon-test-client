export enum Routes {
    LOGIN = 'login',
    REGISTER = 'register',
    USER = 'user',
    ME = 'me',
    TEMPLATE = 'template',
    PDF = 'pdf',
    DOC = 'doc', 
    NOT_FOUND ='not-found'
}