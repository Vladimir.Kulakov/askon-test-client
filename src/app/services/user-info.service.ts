import { Injectable, isDevMode } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { forkJoin, of, Subject, firstValueFrom } from 'rxjs';
import { GridElementTemplate } from '../components/profile-view/class/element.class';
import { UserInfo } from '../dto/user-response.dto';
import { DataService } from './data.service';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {

  private _data: UserInfo = {} as UserInfo;

  public get data() {
    return this._data;
  }

  private _template: GridElementTemplate[] | null = null;

  public get template() {
    return this._template;
  }

  private readonly subject = new Subject<UserInfo | null>();

  private avatarFile?: File;

  public get obs() {
    return this.subject.asObservable();
  }

  private formGroup: FormGroup = new FormGroup({});

  constructor(
    private readonly dataService: DataService, 
    private readonly loginService: LoginService,
  ) {}

  public setControl(field: string): FormControl {
    let control = this.formGroup.controls[field] as FormControl;
    if (!control) {
      control = new FormControl();
      this.formGroup.addControl(field, control);
    }
    return control;
  }

  public removeControl(field: string) {
    let control = this.formGroup.controls[field] as FormControl;
    if (!control) {
      return;
    }
    this.formGroup.removeControl(field);
  }

  public getValue(field: keyof UserInfo) {
    return this._data?.[field];
  }

  public setValue(field: keyof UserInfo, value: any) {
    if (!this._data) {
      return;
    }

    this._data[field] = value;
    this.subject.next(this._data);
  }

  public setAvatar(file:File) {
    this.avatarFile = file;
  }
 

  public load(nick?: string) {
    if (!nick) {
      nick = this.loginService.nickname!;
    }
    return firstValueFrom(forkJoin([
      this.dataService.getUser(nick)?.then(value=> {
        this._data = value?.userInfo || {} as UserInfo;
        this.subject.next(this._data);
      })
      .catch(()=> {
        this._data = {} as UserInfo;
        this.subject.next(this._data);
        return {} as UserInfo;
      }),
      this.dataService.getTemplate(nick)?.then(value=> {
        this._template = value;
      })
    ]))
  }


  public updateInfo() {
    const formData = new FormData();
    if (this.avatarFile) {
      formData.append('avatar', this.avatarFile);
    }

    for (const field of UserInfo.getFields()) {
      const value = this._data[field.title as keyof UserInfo];

      if (!value) {
        continue;
      }
      
      formData.append(field.title, value);
    }

    return this.dataService.updateUser(this.loginService.nickname!, formData);
  }

  public updateTemplate(template: GridElementTemplate[]) {
    return this.dataService.updateTemplate(this.loginService.nickname!, template);
    
  }
}
