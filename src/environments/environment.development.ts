import { BasicEnv } from "./environment.type";

export const environment: BasicEnv = {
    isProd: false,
    apiUrl: "http://172.16.1.4:5179/api"
};
