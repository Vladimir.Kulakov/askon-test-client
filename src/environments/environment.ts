import { BasicEnv } from "./environment.type";

export const environment: BasicEnv = {
    isProd: true,
    apiUrl: "/api"
};
