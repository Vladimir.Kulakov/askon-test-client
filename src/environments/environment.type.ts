export type BasicEnv = {
    isProd: boolean,
    apiUrl: string,
}